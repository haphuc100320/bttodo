import React, { Component } from 'react'
import { Container } from './Components/Container'
import { ThemeProvider } from 'styled-components'
import { ToDoListDarkTheme } from './Themes/ToDoListDarkTheme'
import { ToDoListLightTheme } from './Themes/ToDoListLightTheme'
import { ToDoListPrimaryTheme } from './Themes/ToDoListPrimaryTheme'
import { Dropdown } from './Components/Dropdown'
import { Heading1, Heading2, Heading3, Heading4, Heading5 } from './Components/Heading'
import { label, TextField, Input } from './Components/TextField'
import { Button } from './Components/Button'
import { Table, Tr, Td, Th, Thead, Tbody } from './Components/Table'
import { connect } from 'react-redux'
import { addTaskAction, changeThemeAction, deleteTaskAction, doneTaskAction, editTaskAction, updateTask } from './redux/actions/ToDoListAction'
import { arrTheme } from './Themes/ThemeManager'


class ToDoList extends Component {

  state = {
    taskName: '',
    disabled: true,

  }

  renderTaskToDo = () => {
    return this.props.taskList.filter(task => !task.done).map((task, index) => {
      return <Tr key={index}>
        <Th style={{ verticalAlign: 'middle' }}>{task.taskName}</Th>
        <Th className='text-right'>
          <Button onClick={() => {
            this.setState({
              disabled: false,
            }, () => {
              this.props.dispatch(editTaskAction(task))
            })
          }}>
            <i className='fa fa-edit'></i></Button>
          <Button onClick={() => {
            this.props.dispatch(doneTaskAction(task.id))
          }}>
            <i className='fa fa-check'></i></Button>
          <Button onClick={() => {
            this.props.dispatch(deleteTaskAction(task.id))
          }}>
            <i className='fa fa-trash'></i></Button>
        </Th>
      </Tr>
    })
  }


  renderTaskToCompleted = () => {
    return this.props.taskList.filter(task => task.done).map((task, index) => {
      return <Tr key={index}>
        <Th style={{ verticalAlign: 'middle' }}>{task.taskName}</Th>
        <Th className='text-right'>
          <Button
            onClick={() => {
              this.props.dispatch(deleteTaskAction(task.id))
            }}>
            <i className='fa fa-trash'></i></Button>
        </Th>
      </Tr>
    })
  }

  //Viết hàm render thêm import ThemeManager
  renderTheme = () => {
    return arrTheme.map((theme, index) => {
      return <option value={theme.id}>{theme.name}</option>
    })
  }

  //live cycle bảng 16 nhận vào props mớI đƯợc thực thi trước render
  // componentWillReceiveProps(newProps) {
  //   this.setState({
  //     taskName: newProps.taskEdit.taskName
  //   })
  // }


  render() {
    return (
      <div>
        <ThemeProvider theme={this.props.themeToDoList} >
          <Container className='w-50 text-left'>
            <Dropdown onChange={(e) => {
              let { value } = e.target;
              //dispatch value lên reducer

              this.props.dispatch(changeThemeAction(value))
            }}>
              {this.renderTheme()}
            </Dropdown>
            <Heading3 >To do list</Heading3>
            <TextField value={this.state.taskName} onChange={(e) => {
              this.setState({
                taskName: e.target.value
              },

              )
            }}
              name='taskName' label="Task name" className='w-50'></TextField>
            <button
              onClick={() => {
                //lấy thông tin người dùng nhập vào input
                let { taskName } = this.state
                // tạo ra 1 task object
                let newTask = {
                  id: Date.now(),
                  taskName: taskName,
                  done: false
                }
                //đưa task object lên redux thông qua phương thức pispatch
                this.props.dispatch(addTaskAction(newTask))
              }}
              className='ml-2'> <i className='fa fa-plus'></i> Add task</button>
            {
              this.state.disabled ?
                <button disabled
                  onClick={() => {
                    this.props.dispatch(updateTask(this.state.taskName))
                  }}
                  className='ml-2'> <i class="fa fa-upload"></i> Update task</button> :
                <button
                  onClick={() => {
                    let { taskName } = this.state
                    this.setState({
                      disabled: true,
                      taskName: ''
                    }, () => {
                      this.props.dispatch(updateTask(taskName))
                    })
                  }}
                  className='ml-2'> <i class="fa fa-upload"></i> Update task</button>
            }

            <hr />
            <Heading2>Task to do</Heading2>
            <Table>
              <Thead>
                {this.renderTaskToDo()}
              </Thead>
            </Table>

            <Heading2>Task completed</Heading2>
            <Table>
              <Thead>
                {this.renderTaskToCompleted()}
              </Thead>
            </Table>

          </Container>
        </ThemeProvider >
      </div>

    )
  }

  //đây là lifecycle trả về props cũ và state cũ của component trước khi render (lifecycle này chạy sau render)
  componentDidUpdate(prevProps, prevState) {
    if (prevProps.taskEdit.id !== this.props.taskEdit.id) {
      this.setState({
        // so sánh nếu như props trước đÓ( taskEdit trước mà khác taskEdit hiện tại thì mình mới setState)
        taskName: this.props.taskEdit.taskName
      })
    }
  }

}

const mapStateToProps = (state) => {
  return {
    themeToDoList: state.ToDoListReducer.themeToDoList,
    taskList: state.ToDoListReducer.taskList,
    taskEdit: state.ToDoListReducer.taskEdit
  }
}

export default connect(mapStateToProps)(ToDoList)

